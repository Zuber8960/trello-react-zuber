import React from "react";
import Navbar from "./Components/Navbar";
import Boards from "./Components/Boards";
import { Route, Routes } from "react-router-dom";
import Lists from "./Components/Lists";
import ErrorPage from "./Components/ErrorPage";
import AlertDialog from "./Components/AlertDialog";

const App = () => {

  return (
    <>
      <Navbar />
      <Routes>
        <Route path="/" exact element={<Boards />} />
        <Route path="/boards/:boardId" element={<Lists />} />
        <Route path="*" element={<ErrorPage />} />
        <Route />
      </Routes>
    </>
  );
};

export default App;
