import { configureStore } from "@reduxjs/toolkit";
import boardSlice from "../slices/boardSlice";
import listSlice from "../slices/listSlice";
import cardSlice from "../slices/cardSlice";
import checklistSlice from "../slices/checklistSlice";

const store = configureStore({
    reducer : {
        boardsData : boardSlice,
        listsData : listSlice,
        cardsData : cardSlice,
        checklistsData : checklistSlice
    }
});


export default store;