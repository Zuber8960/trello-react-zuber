import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { createNewCardApi, deleteACardApi, getAllCardsOfSingleBoardApi } from "../Components/apiLinks";


export const getAllCardsOfSingleBoardResponce = createAsyncThunk(
    "list/getAllCards", (boardId) => getAllCardsOfSingleBoardApi(boardId)
);


export const createACardResponce = createAsyncThunk(
    "list/createACard", ({listId,cardName}) => {
        return createNewCardApi(listId,cardName);
    }
);

export const deleteACardResponce = createAsyncThunk(
    "list/deleteACard", async({cardId}) => {
        await deleteACardApi(cardId);
        return cardId;
    }
);

const initialState = {
    card : [],
    error : ""
}

const cardSlice = createSlice({
    name : "card",
    initialState,
    reducers : {},
    extraReducers: (builder) => {
        builder.addCase(getAllCardsOfSingleBoardResponce.fulfilled, (state, action) => {
          state.card = action.payload;
          state.error = "";
        });
        builder.addCase(getAllCardsOfSingleBoardResponce.pending, (state, action) => {
          state.error = "";
        });
        builder.addCase(getAllCardsOfSingleBoardResponce.rejected, (state, action) => {
          state.error =
            "An error occurred while fetching cards. Please try again later.";
        });
        builder.addCase(createACardResponce.fulfilled, (state, action) => {
            state.card.push(action.payload);
            state.error = "";
        });
        builder.addCase(createACardResponce.pending, (state, action) => {
            state.error = "";
        });
        builder.addCase(createACardResponce.rejected, (state, action) => {
            state.error = "An error occurred while creating a card. Please try again later.";
        });
        builder.addCase(deleteACardResponce.fulfilled, (state, action) => {
            const cardId = action.payload;

            const remainsCards = state.card.filter((cardData) => {
                return cardData.id !== cardId;
            });

            state.card = remainsCards;
            state.error = "";
        });
        builder.addCase(deleteACardResponce.pending, (state, action) => {
            state.error = "";
        });
        builder.addCase(deleteACardResponce.rejected, (state, action) => {
            state.error = "An error occurred while deleting a card. Please try again later.";
        });
      },
});


export default cardSlice.reducer;