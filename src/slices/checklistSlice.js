import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { createAChecklistApi, createCheckitemApi, deleteACheckitemInCheckListApi, deleteAChecklistApi, getAllChecklistsInACardApi, toggleCheckItemApi } from "../Components/apiLinks";


export const getAllChecklistsOfSingleCardResponce = createAsyncThunk(
    "checklist/getAllChecklists", ({cardId}) => getAllChecklistsInACardApi(cardId)
);


export const createANewChecklistResponce = createAsyncThunk(
    "checklist/createNewChecklist", ({cardId, checkListName}) => createAChecklistApi(cardId,checkListName)
);


export const deleteAChecklistResponce = createAsyncThunk(
    "checklist/deleteAChecklist", async({cardId, checklistId}) => {
        await deleteAChecklistApi(cardId,checklistId)
        return checklistId;
    }
);


export const createACheckItemResponce = createAsyncThunk(
    "checklist/createACheckItem", async ({checklistId, checkItemName, checkItemState}) => {
        const result = await createCheckitemApi(checklistId, checkItemName, checkItemState);
        return {data : result, checklistId : checklistId};
    }
);


export const deleteACheckItemResponce = createAsyncThunk(
    "checklist/deleteACheckItem", async ({checklistId, checkItemId}) => {
        await deleteACheckitemInCheckListApi(checklistId, checkItemId);
        return {checklistId : checklistId, checkItemId:checkItemId};
    }
);


export const toggleCheckItemResponce = createAsyncThunk(
    "checklist/toggleCheckItem", async ({cardId, checkItem}) => {
        await toggleCheckItemApi(cardId, checkItem);
        return {checkItemId:checkItem.id};
    }
)



const initialState = {
    checklist : [],
    error : ""
}

const checklistSlice = createSlice({
    name : "checklist",
    initialState,
    reducers : {},
    extraReducers: (builder) => {
        builder.addCase(getAllChecklistsOfSingleCardResponce.fulfilled, (state, action) => {
          state.checklist = action.payload;
          state.error = "";
        });
        builder.addCase(getAllChecklistsOfSingleCardResponce.pending, (state, action) => {
          state.error = "";
        });
        builder.addCase(getAllChecklistsOfSingleCardResponce.rejected, (state, action) => {
          state.error =
            "An error occurred while fetching chekclists. Please try again later.";
        });
        builder.addCase(createANewChecklistResponce.fulfilled, (state, action) => {
            state.checklist.push(action.payload);
            state.error = "";
        });
        builder.addCase(createANewChecklistResponce.pending, (state, action) => {
            state.error = "";
        });
        builder.addCase(createANewChecklistResponce.rejected, (state, action) => {
            state.error =
            "An error occurred while creating chekclists. Please try again later.";
        });
        builder.addCase(deleteAChecklistResponce.fulfilled, (state, action) => {
            const checklistId = action.payload;

            const remainCheckList = state.checklist.filter((checklistData) => {
                return checklistData.id !== checklistId;
            });

            state.checklist = remainCheckList;
            state.error = "";
        });
        builder.addCase(deleteAChecklistResponce.pending, (state, action) => {
            state.error = "";
        });
        builder.addCase(deleteAChecklistResponce.rejected, (state, action) => {
            state.error =
            "An error occurred while deleting a chekclists. Please try again later.";
        });
        builder.addCase(createACheckItemResponce.fulfilled, (state, action) => {
            const newChecklist = action.payload.data;
            const checklistId = action.payload.checklistId;

            const newChecklistArr = state.checklist.reduce((arr, { ...curr }) => {
                if (curr.id === checklistId) {
                  curr.checkItems = [...curr.checkItems, newChecklist];
                }
                arr.push(curr);
                return arr;
              }, []);

            state.checklist = newChecklistArr;
            state.error = "";
        });
        builder.addCase(createACheckItemResponce.pending, (state, action) => {
            state.error = "";
        });
        builder.addCase(createACheckItemResponce.rejected, (state, action) => {
            state.error =
            "An error occurred while creating a chekcItem. Please try again later.";
        });
        builder.addCase(deleteACheckItemResponce.fulfilled, (state, action) => {
            const checklistId = action.payload.checklistId;
            const checkItemId = action.payload.checkItemId;

            const newChecklistArr = state.checklist.map(({ ...checklist }) => {
                if(checklist.id === checklistId){
                    const result = checklist.checkItems.reduce((arr, { ...curr }) => {
                        if (curr.id !== checkItemId) {
                          arr.push(curr);
                        }
                        return arr;
                      }, []);
                      checklist.checkItems = result;
                }
                return checklist;
              });

            state.checklist = newChecklistArr;
            state.error = "";
        });
        builder.addCase(deleteACheckItemResponce.pending, (state, action) => {
            state.error = "";
        });
        builder.addCase(deleteACheckItemResponce.rejected, (state, action) => {
            state.error =
            "An error occurred while deleting a chekcItem. Please try again later.";
        });
        builder.addCase(toggleCheckItemResponce.fulfilled, (state, action) => {
            const checklistsArr = state.checklist;
            const checkItemId = action.payload.checkItemId;

            const newChecklistArr = checklistsArr.map(({ ...checklist }) => {
                const itemResults = checklist.checkItems.reduce((arr, { ...curr }) => {
                    if (curr.id === checkItemId) {
                        curr.state =
                        curr.state === "complete" ? "incomplete" : "complete";
                    }
                    arr.push(curr);
                    return arr;
                    },[]);

                checklist.checkItems = itemResults;
                return checklist;
            })

            state.checklist = newChecklistArr;
            state.error = "";
        });
        builder.addCase(toggleCheckItemResponce.pending, (state, action) => {
            state.error = "";
        });
        builder.addCase(toggleCheckItemResponce.rejected, (state, action) => {
            state.error =
            "An error occurred while deleting a chekcItem. Please try again later.";
        });
      },
});


export default checklistSlice.reducer;