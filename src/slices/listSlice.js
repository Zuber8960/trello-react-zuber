import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { createNewListApi, deleteAListApi, getAllListsOfSingleBoardApi } from "../Components/apiLinks";

const initialState = {
    list : [],
    loader : true,
    error : ""
};

export const getAllListsOfSingleBoardResponce = createAsyncThunk(
    "list/getAllLists" , (boardId) => getAllListsOfSingleBoardApi(boardId)
);

export const createNewListResponce = createAsyncThunk(
    "list/createNewList" , ({boardId, listName}) => {
        return createNewListApi(boardId, listName);
    }
);


export const deleteAListResponce = createAsyncThunk(
    "list/deleteAList" , (listId) => {
        return deleteAListApi(listId);
    }
);




const listSlice = createSlice({
    name : "list",
    initialState,
    reducers : {},
    extraReducers: (builder) => {
        builder.addCase(getAllListsOfSingleBoardResponce.fulfilled, (state, action) => {
          state.list = action.payload;
          state.loader = false;
          state.error = "";
        });
        builder.addCase(getAllListsOfSingleBoardResponce.pending, (state, action) => {
          state.loader = true;
          state.error = "";
        });
        builder.addCase(getAllListsOfSingleBoardResponce.rejected, (state, action) => {
          state.loader = false;
          state.error =
            "An error occurred while fetching lists. Please try again later.";
        });
        builder.addCase(createNewListResponce.fulfilled, (state, action) => {
        console.log(action.payload);
          state.list.push(action.payload);
          state.error = "";
        });
        builder.addCase(createNewListResponce.pending, (state, action) => {
          state.error = "";
        });
        builder.addCase(createNewListResponce.rejected, (state, action) => {
          state.error =
            "An error occurred while creating a list. Please try again later.";
        });
        builder.addCase(deleteAListResponce.fulfilled, (state, action) => {
            const listId = action.payload.id;

            const remainsList = state.list.filter((listData) => {
                return listData.id !== listId;
            });

            state.list = remainsList;
            state.error = "";
        });
        builder.addCase(deleteAListResponce.pending, (state, action) => {
            state.error = "";
        });
        builder.addCase(deleteAListResponce.rejected, (state, action) => {
            state.error =
            "An error occurred while deleting a list. Please try again later.";
        });
      },

});


export default listSlice.reducer;