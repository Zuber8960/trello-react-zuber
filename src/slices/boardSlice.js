import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

import { createNewBoardApi, getAllBoardsApi } from "../Components/apiLinks";

export const getAllBoardsDataResponce = createAsyncThunk(
  "board/getAllBoardData", () => getAllBoardsApi()
);

export const createNewBoardResponce = createAsyncThunk(
  "board/createNewBoard", (name) => createNewBoardApi(name)
);

const initialState = {
  board: [],
  loader: true,
  error: "",
};

const boardSlice = createSlice({
  name: "board",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getAllBoardsDataResponce.fulfilled, (state, action) => {
      // console.log("this is fulfilled state");
      state.board = action.payload;
      state.loader = false;
      state.error = "";
    });
    builder.addCase(getAllBoardsDataResponce.pending, (state, action) => {
      // console.log("this is pending state");
      state.loader = true;
      state.error = "";
    });
    builder.addCase(getAllBoardsDataResponce.rejected, (state, action) => {
      //   console.log("this is error in board");
      state.loader = false;
      state.error =
        "An error occurred while fetching boards. Please try again later.";
    });
    builder.addCase(createNewBoardResponce.fulfilled, (state, action) => {
      state.board.push(action.payload);
      state.error = "";
    });
    builder.addCase(createNewBoardResponce.pending, (state, action) => {
      state.error = "";
    });
    builder.addCase(createNewBoardResponce.rejected, (state, action) => {
      state.error =
        "An error occurred while creating a board. Please try again later.";
    });
  },
});

export default boardSlice.reducer;
