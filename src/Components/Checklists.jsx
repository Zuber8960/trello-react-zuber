import * as React from "react";
import PropTypes from "prop-types";
import { styled } from "@mui/material/styles";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";

import { CardActionArea, Stack } from "@mui/material";
import MyPopover from "./MyPopover";

import Items from "./Items";
import { useDispatch, useSelector } from "react-redux";
import { createANewChecklistResponce, getAllChecklistsOfSingleCardResponce } from "../slices/checklistSlice";

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2),
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1),
  },
}));

function BootstrapDialogTitle(props) {
  const { children, onClose, ...other } = props;

  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: "absolute",
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
}

BootstrapDialogTitle.propTypes = {
  children: PropTypes.node,
  onClose: PropTypes.func.isRequired,
};

export default function CheckLists({ cardName, cardId }) {
  const [open, setOpen] = React.useState(false);

  const dispatch = useDispatch();

  const checklistsArr = useSelector((state) => state.checklistsData.checklist);
  // console.log(checklistsArr);

  function gettingAllChecklists(cardId) {
    dispatch(getAllChecklistsOfSingleCardResponce({cardId}));
  }

  // function for create a checkList
  function createChecklistHandler(event, handleClose) {
    event.preventDefault();

    const checkListName = event.target.inputFeild.value;
    dispatch(createANewChecklistResponce({cardId, checkListName}));
    handleClose();
  }

  const handleClickOpen = () => {
    setOpen(true);

    gettingAllChecklists(cardId);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div style={{ flexGrow: 1 }}>
      <CardActionArea
        onClick={handleClickOpen}
        sx={{ fontSize: "18px", color: "gray" }}
      >
        {cardName}
      </CardActionArea>
      <BootstrapDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <BootstrapDialogTitle
          id="customized-dialog-title"
          onClose={handleClose}
        >
          {cardName}
        </BootstrapDialogTitle>
        <DialogContent dividers sx={{ height: "70vh", width: "70vh" }}>
          <MyPopover
            buttonTxt="+ Add Checklist"
            buttonName="Checklist"
            cb={createChecklistHandler}
          />

          {checklistsArr.map((checklist) => {
            return (
              <Stack
                sx={{
                  margin: "8px 0",
                  border: "1px solid gray",
                  borderRadius: "5px",
                  padding: "5px",
                }}
                key={checklist.id}
              >
                <Items
                  cardId={cardId}
                  checklistId={checklist.id}
                  checklistName={checklist.name}
                />
              </Stack>
            );
          })}
        </DialogContent>
      </BootstrapDialog>
    </div>
  );
}
