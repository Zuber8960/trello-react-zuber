import axios from "axios";

const { VITE_APIKEY, VITE_TOKEN } = import.meta.env;
const BASE_URL = "https://api.trello.com/1";

export const getAllBoardsApi = async () => {
    const result = await axios.get(
      `${BASE_URL}/members/me/boards?key=${VITE_APIKEY}&token=${VITE_TOKEN}`
    );
    return result.data;
};

export const createNewBoardApi = async (name) => {
    const result = await axios.post(
      `${BASE_URL}/boards/?name=${name}&key=${VITE_APIKEY}&token=${VITE_TOKEN}`
    );
    return result.data;
};

export const getAllListsOfSingleBoardApi = async (id) => {
    const result = await axios.get(
      `${BASE_URL}/boards/${id}/lists?key=${VITE_APIKEY}&token=${VITE_TOKEN}`
    );
    return result.data;
};


export const getAllCardsOfSingleBoardApi = async (id) => {
    const result = await axios.get(
      `${BASE_URL}/boards/${id}/cards?key=${VITE_APIKEY}&token=${VITE_TOKEN}`
    );
    return result.data ;
};

export const createNewListApi = async (boardId, listName) => {
    const result = await axios.post(
      `${BASE_URL}/boards/${boardId}/lists?name=${listName}&key=${VITE_APIKEY}&token=${VITE_TOKEN}`
    );
    return result.data;
};

export const createNewCardApi = async (listId, cardName) => {
    const result = await axios.post(
      `${BASE_URL}/cards?idList=${listId}&name=${cardName}&key=${VITE_APIKEY}&token=${VITE_TOKEN}`
    );
    return result.data ;
};

export const deleteAListApi = async (listId) => {
    const result = await axios.put(
      `${BASE_URL}/lists/${listId}?closed=true&name=name&key=${VITE_APIKEY}&token=${VITE_TOKEN}`
    );
    return result.data;
};

export const deleteACardApi = async (cardId) => {
    const result = await axios.delete(
      `${BASE_URL}/cards/${cardId}?key=${VITE_APIKEY}&token=${VITE_TOKEN}`
    );
    return result.data;
};

export const getAllChecklistsInACardApi = async (cardId) => {
    const result = await axios.get(
      `${BASE_URL}/cards/${cardId}/checklists?key=${VITE_APIKEY}&token=${VITE_TOKEN}`
    );
    return result.data;
};

export const createAChecklistApi = async (cardId, checkListName) => {
    const result = await axios.post(
      `${BASE_URL}/cards/${cardId}/checklists?name=${checkListName}&key=${VITE_APIKEY}&token=${VITE_TOKEN}`
    );
    return result.data;
};

export const deleteAChecklistApi = async (cardId, checkListId) => {
    const result = await axios.delete(
      `${BASE_URL}/cards/${cardId}/checklists/${checkListId}?key=${VITE_APIKEY}&token=${VITE_TOKEN}`
    );
    return result.data ;
};

export const createCheckitemApi = async (
  checklistId,
  checkItemName,
  checkItemState
) => {
    const result = await axios.post(
      `${BASE_URL}/checklists/${checklistId}/checkItems?name=${checkItemName}&checked=${
        checkItemState === "incomplete"
      }&key=${VITE_APIKEY}&token=${VITE_TOKEN}`
    );
    return result.data;
};

export const deleteACheckitemInCheckListApi = async (
  checklistId,
  checkItemid
) => {
    const result = await axios.delete(
      `${BASE_URL}/checklists/${checklistId}/checkItems/${checkItemid}?key=${VITE_APIKEY}&token=${VITE_TOKEN}`
    );
    return result.data;
};

export const toggleCheckItemApi = async (cardId, checkitem) => {
    const result = await axios.put(
      `${BASE_URL}/cards/${cardId}/checkItem/${checkitem.id}?key=${VITE_APIKEY}&token=${VITE_TOKEN}`,
      {
        state: checkitem.state === "complete" ? "incomplete" : "complete",
      }
    );
    return result.data;
};
