import * as React from "react";
import Box from "@mui/material/Box";
import Checkbox from "@mui/material/Checkbox";
import FormControlLabel from "@mui/material/FormControlLabel";
import ConfirmDelete from "./ConfirmDelete";

import { FormControl, Stack, Typography } from "@mui/material";
import MyPopover from "./MyPopover";
import { useDispatch, useSelector } from "react-redux";
import { createACheckItemResponce, deleteACheckItemResponce, deleteAChecklistResponce, toggleCheckItemResponce } from "../slices/checklistSlice";
import AlertDialog from "./AlertDialog";

export default function Items({ cardId, checklistId, checklistName }) {
  const checklistsArr = useSelector((state) => state.checklistsData.checklist);
  const checklistError = useSelector(state => state.checklistsData.error);

  const dispatch = useDispatch();

  // create a checkItem function
  async function createitemHandler(event, handleClose, checkItemState) {

    event.preventDefault();
    const checkItemName = event.target.inputFeild.value;
    dispatch(createACheckItemResponce({checklistId,checkItemName, checkItemState}));
    handleClose();
  }

  // delete a checklist function

  function deleteChecklist(customData, handleClose) {
    const { cardId, checklistId } = customData;
    dispatch(deleteAChecklistResponce({cardId, checklistId}));
    handleClose();
  }

  // delete a checkedItem function

  function deleteCheckedItem(customData, handleClose) {

    const { checklistId, checkItemId } = customData;
    dispatch(deleteACheckItemResponce({checklistId, checkItemId}))
    handleClose();
  }

  // toggle checkItem function
  const handleChange = async (event) => {
    const checkItem = checklistsArr
        .find((checklist) => checklist.id === checklistId)
        .checkItems.find((item) => item.id === event.target.id);
    
    dispatch(toggleCheckItemResponce({cardId, checkItem}));
  };

  const itemArr = checklistsArr.find((checklist) => {
    return checklist.id === checklistId;
  });

  const children = (
    <Box sx={{ display: "flex", flexDirection: "column", ml: 3 }}>
      {(itemArr &&
        itemArr.checkItems.map((checkItem) => {
          return (
            <FormControl
              key={checkItem.id}
              sx={{
                flexDirection: "row",
                justifyContent: "space-between",
                mr: 5,
              }}
            >
              <FormControlLabel
                label={checkItem.name}
                control={
                  <Checkbox
                    checked={(checkItem.state == "complete" && true) || false}
                    onClick={handleChange}
                    id={checkItem.id}
                  />
                }
              />
              <ConfirmDelete
                cb={deleteCheckedItem}
                name={checkItem.name}
                type="item"
                customData={{
                  checklistId: checklistId,
                  checkItemId: checkItem.id,
                }}
              />
            </FormControl>
          );
        })) ||
        null}
      <MyPopover
        buttonTxt="+ Add An Item"
        buttonName="Item"
        cb={createitemHandler}
        custumData={false}
      />
    </Box>
  );

  return (
    <div>
      {checklistError && <AlertDialog message={checklistError} />}
      <Stack sx={{ flexDirection: "row", justifyContent: "space-between" }}>
        <Typography>{checklistName}</Typography>
        <ConfirmDelete
          cb={deleteChecklist}
          name={checklistName}
          type="checklist"
          customData={{ cardId: cardId, checklistId: checklistId }}
        />
      </Stack>

      {children}
    </div>
  );
}