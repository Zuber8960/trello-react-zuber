import React, { useEffect } from "react";
import { useLocation, useParams } from "react-router-dom";
import { Stack, Typography, Toolbar, CircularProgress } from "@mui/material";

import CardsInList from "./CardsInList";

import { useDispatch, useSelector } from "react-redux";
import { getAllListsOfSingleBoardResponce } from "../slices/listSlice";
import { getAllCardsOfSingleBoardResponce } from "../slices/cardSlice";

import AlertDialog from "./AlertDialog";

const Lists = () => {

  const dispatch = useDispatch();

  const loader = useSelector((state) => state.listsData.loader);
  const listError = useSelector((state) => state.listsData.error);
  const cardError = useSelector((state) => state.cardsData.error);

  const { boardId } = useParams();
  const location = useLocation();
  const { name, img, color } = location.state;

  useEffect(() => {
    dispatch(getAllListsOfSingleBoardResponce(boardId));
    dispatch(getAllCardsOfSingleBoardResponce(boardId));
  }, []);

  return (
    <>
      {(listError && <AlertDialog message={listError} />) ||
        (cardError && <AlertDialog message={cardError} />)}

      {(loader && (
        <CircularProgress
          color="primary"
          size={150}
          thickness={5}
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
          }}
        />
      )) || (
        <Stack
          spacing={2}
          sx={{
            backgroundImage: `url(${img})`,
            backgroundColor: color,
            backgroundSize: "cover",
            backgroundPosition: "center",
          }}
        >
          <Toolbar sx={{ background: "rgba(0,0,0,0.5)" }}>
            <Stack>
              <Typography variant="h4" color="#ffffff">
                {name}
              </Typography>
            </Stack>
          </Toolbar>
          <Stack>
            <CardsInList />
          </Stack>
        </Stack>
      )}
    </>
  );
};

export default Lists;
