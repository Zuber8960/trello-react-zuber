import * as React from "react";
import { Card, Popover, Button, TextField } from "@mui/material";
import { useSelector } from "react-redux";

export default function MyPopover({ buttonTxt, buttonName, cb, custumData }) {
  const boardsData = useSelector((state) => state.boardsData.board);

  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleClick = (event) => {
    if (buttonName === "Board") {
      if (10 - boardsData.length > 0) {
        setAnchorEl(event.currentTarget);
      }
    } else {
      setAnchorEl(event.currentTarget);
    }
  };

  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;

  return (
    <>
      <Button
        aria-describedby={id}
        variant="contained"
        onClick={handleClick}
        sx={{ alignSelf: "center" }}
      >
        {buttonTxt}
      </Button>

      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
      >
        <Card sx={{ width: 200 }}>
          <form onSubmit={(event) => cb(event, handleClose, custumData)}>
            <TextField
              placeholder={`Enter ${buttonName} Name...`}
              id="inputFeild"
              required
            />
            <Button
              type="submit"
              color="primary"
              variant="contained"
              sx={{ m: "5px" }}
            >
              Create {buttonName}
            </Button>
          </form>
        </Card>
      </Popover>
    </>
  );
}
