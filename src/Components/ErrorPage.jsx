import React from "react";
import { Typography, Container, Box, Button } from "@mui/material";
import { Link } from 'react-router-dom';
import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';

const ErrorPage = () => {
  return (
    <Container>
      <Box textAlign="center" pt={10}>
        <ErrorOutlineIcon sx={{ fontSize: 100, color: 'error' }} />
        <Typography variant="h4" color="error" mt={2}>
          Oops! Something went wrong.
        </Typography>
        <Typography variant="h6" color="textSecondary" mt={2}>
          The page you are looking for could not be found.
        </Typography>
        <Button variant="contained" component={Link} to="/" color="primary" mt={4}>
          Home Page
        </Button>
      </Box>
    </Container>
  );
};

export default ErrorPage;
