import React from "react";
import { Card, Typography, Paper, Stack } from "@mui/material";

import MyPopover from "./MyPopover";
import ConfirnDelete from "./ConfirmDelete";
import CheckLists from "./Checklists";

import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { createNewListResponce, deleteAListResponce } from "../slices/listSlice";
import { createACardResponce, deleteACardResponce } from "../slices/cardSlice";

const CardsInList = () => {
  const lists = useSelector((state) => state.listsData.list);
  const cardsArr = useSelector((state) => state.cardsData.card);
  // console.log(cardsArr);

  const dispatch = useDispatch();

  const { boardId } = useParams();

  // function for create a list
  async function createListHandler(event, handleClose) {
    event.preventDefault();
    const listName = event.target.inputFeild.value;
    console.log(listName);
    dispatch(createNewListResponce({boardId, listName}));
    handleClose();
  }

  // delete a list function

  async function deleteList(customData, handleClose) {
    const {listId} = customData;
    dispatch(deleteAListResponce(listId));
    handleClose();
  }

  // function for create a card
  async function createCardHandler(event, handleClose, listId) {
    event.preventDefault();
    const cardName = event.target.inputFeild.value;
    dispatch(createACardResponce({listId, cardName}));
    handleClose();
  }

  // delete a card function

  async function deleteCard(customData, handleClose) {
    const { cardId } = customData;
    dispatch(deleteACardResponce({cardId}));
    handleClose();
  }

  return (
    <Stack
      sx={{
        flexDirection: "row",
        gap: "1em",
        alignItems: "flex-start",
        overflowX: "scroll",
        height: "75vh",
      }}
    >
      {(lists.length &&
        lists.map((list) => {
          return (
            <Card
              sx={{
                backgroundColor: "#f1f2f4",
                padding: "15px",
                flexShrink: "0",
              }}
              key={list.id}
            >
              <Stack
                sx={{ flexDirection: "row", justifyContent: "space-between" }}
              >
                <Typography variant="h6">{list.name}</Typography>
                <ConfirnDelete
                  cb={deleteList}
                  name={list.name}
                  type="list"
                  customData={{ listId: list.id }}
                />
              </Stack>
              {(cardsArr.length &&
                cardsArr
                  .filter((card) => {
                    return card.idList == list.id;
                  })
                  .map((card) => {
                    return (
                      <Paper
                        key={card.id}
                        sx={{
                          margin: "5px 0",
                          display: "flex",
                          padding: "3px",
                        }}
                      >
                        <CheckLists cardId={card.id} cardName={card.name} />
                        <ConfirnDelete
                          name={card.name}
                          cb={deleteCard}
                          customData={{ cardId: card.id }}
                          type="card"
                        />
                      </Paper>
                    );
                  })) ||
                null}

              <MyPopover
                buttonTxt="+ Add A Card"
                buttonName="Card"
                cb={createCardHandler}
                custumData={list.id}
              ></MyPopover>
            </Card>
          );
        })) ||
        null}
      <Card sx={{ backgroundColor: "#3d7da2", flexShrink: "0" }}>
        <MyPopover
          buttonTxt="+ Add Another List"
          buttonName="List"
          cb={createListHandler}
        ></MyPopover>
      </Card>
    </Stack>
  );
};

export default CardsInList;