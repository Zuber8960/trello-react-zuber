import { useEffect } from "react";
import { Card, CardActionArea, Stack, Typography } from "@mui/material";
import { Link } from "react-router-dom";

import MyPopover from "./MyPopover";

import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import { CircularProgress } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import {
  createNewBoardResponce,
  getAllBoardsDataResponce,
} from "../slices/boardSlice";
import AlertDialog from "./AlertDialog";

function Boards() {
  const boardsData = useSelector((state) => state.boardsData.board);
  const dispatch = useDispatch();

  const loader = useSelector(state => state.boardsData.loader);
  const error = useSelector(state => state.boardsData.error);

  // function for create a board
  async function createBoardHandler(event, handleClose) {
    event.preventDefault();
    
    const boardName = event.target.inputFeild.value;
    dispatch(createNewBoardResponce(boardName));
    handleClose();
  }

  useEffect(() => {
    dispatch(getAllBoardsDataResponce());
  }, []);

  return (
    <>
      {error && <AlertDialog message={error} />}
      {(loader && (
        <CircularProgress
          size={150}
          color="primary"
          thickness={5}
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
          }}
        />
      )) || (
        <Stack
          flexDirection="row"
          flexWrap="wrap"
          gap="1em"
          margin="1em"
          justifyContent="center"
        >
          {(boardsData.length &&
            boardsData.map((board) => {
              return (
                <Link
                  to={`/boards/${board.id}`}
                  state={{
                    name: board.name,
                    img: board.prefs.backgroundImage || null,
                    color: board.prefs.backgroundColor || null,
                  }}
                  key={board.id}
                >
                  <Card
                    sx={{
                      width: 300,
                      height: 180,
                      backgroundColor: board.prefs.backgroundColor,
                      backgroundImage: `url(${board.prefs.backgroundImage})`,
                      display: "flex",
                      justifyContent: "center",
                      backgroundSize: "cover",
                      backgroundPosition: "center",
                    }}
                  >
                    <CardActionArea
                      sx={{
                        display: "flex",
                        justifyContent: "center",
                        flexDirection: "column",
                        color: "#ffffff",
                        fontSize: "30px",
                      }}
                    >
                      {board.name}
                    </CardActionArea>
                  </Card>
                </Link>
              );
            })) ||
            null}
          <Card sx={{ backgroundColor: "#282d33", width: 300, height: 180 }}>
            <Stack
              sx={{
                display: "flex",
                justifyContent: "center",
                flexDirection: "column",
                color: "gray",
                height: "100%",
                textAlign: "center",
              }}
            >
              {(10 - boardsData.length > 0 && (
                <Typography sx={{ fontSize: "20px" }}>
                  Create New Board
                </Typography>
              )) ||
                null}
              {(10 - boardsData.length > 0 && (
                <AddCircleOutlineIcon sx={{ alignSelf: "center" }} />
              )) ||
                null}
              <Typography>{10 - boardsData.length} remaining</Typography>
              <MyPopover
                buttonTxt="Create"
                buttonName="Board"
                cb={createBoardHandler}
              ></MyPopover>
            </Stack>
          </Card>
        </Stack>
      )}
    </>
  );
}

export default Boards;
