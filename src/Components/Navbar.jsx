import React, { useContext, useReducer, useState } from "react";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import { Link } from "react-router-dom";
import MyPopover from "./MyPopover";
import { useDispatch } from "react-redux";
import { createNewBoardResponce } from "../slices/boardSlice";

const NavBar = () => {
  const dispatch = useDispatch();

  const [isPlaying, setIsPlaying] = useState(false);

  const createBoardHandler = async (event, handleClose) => {
    event.preventDefault();
    const boardName = event.target.inputFeild.value;
    dispatch(createNewBoardResponce(boardName));
    handleClose();
  };

  return (
    <AppBar position="relative" sx={{ backgroundColor: "#1d2125" }}>
      <Toolbar>
        <Link to="/">
          <img
            src={
              isPlaying
                ? "https://trello.com/assets/87e1af770a49ce8e84e3.gif"
                : "https://trello.com/assets/d947df93bc055849898e.gif"
            }
            alt="icon"
            onMouseEnter={() => setIsPlaying(true)}
            onMouseLeave={() => setIsPlaying(false)}
            className="hoverable-icon"
            style={{ height: "22px", width: "100px", marginRight: "8px" }}
          />
        </Link>

        <MyPopover
          buttonTxt="Create"
          buttonName="Board"
          cb={createBoardHandler}
        />
      </Toolbar>
    </AppBar>
  );
};

export default NavBar;
